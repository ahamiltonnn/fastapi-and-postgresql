from fastapi import APIRouter, Depends
from queries.vacations import VacationIn, VacationRepository


router = APIRouter()

@router.post("/vacations")
def create_vacation(vacation: VacationIn, repo: VacationRepository = Depends()):
    return repo.create(vacation)
    
